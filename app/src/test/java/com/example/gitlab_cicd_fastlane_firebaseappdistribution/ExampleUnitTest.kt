package com.example.gitlab_cicd_fastlane_firebaseappdistribution

import org.junit.Test

import org.junit.Assert.*

/**
 *
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(5, 2 + 2)
    }
}
